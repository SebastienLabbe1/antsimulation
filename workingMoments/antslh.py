#!/usr/bin/env python3
import numpy as np
import random as rd
import pygame,time

class Ant:
    def __init__(self, pos, dpos, world):
        self.hasFood = False
        self.pos = pos
        self.dpos = dpos
        self.world = world

    def update(self, dpos):
        self.dpos = dpos
        self.pos += self.dpos

    def getSur(self, table):
        bMin,bMax = self.world.crop(self.pos - 1),self.world.crop(self.pos + 1)
        vision = table[bMin[0]:bMax[0]+1,bMin[1]:bMax[1]+1]
        isx0, isy0 = int(not self.pos[0]),int(not self.pos[1])
        isxmax, isymax = int(self.pos[0] == self.world.size[0]-1),int(self.pos[1] == self.world.size[1]-1)
        moves = [((i,j),vision[(1-isx0)+i,(1-isy0)+j]) for i in range(isx0-1,2-isxmax) for j in range(isy0-1,2-isymax) if i!=0 and j!=0]
        return moves

    def move(self):
        table = (self.world.pher if self.hasFood else self.world.food)
        moves = self.getSur(table)
        x = sum(x[1] for x in moves) + len(moves) * self.world.baseWeight
        y = rd.random() * x
        i = -1
        while y >= 0 and i < len(moves)-1: 
            y -= moves[i+1][1] + self.world.baseWeight
            i += 1
        self.update(moves[i][0])
        if not self.foodAction():
            self.world.addPher(self.pos)

    def foodAction(self):
        foodCount = sum(x[1] for x in self.getSur(self.world.food))
        foundFood = self.world.getFood(self.pos)
        if foodCount < rd.random() * 8:
            if not self.hasFood and foundFood:
                self.hasFood = True
                self.world.setFood(self.pos, 0)
                return True
        else:
            if self.hasFood and not foundFood:
                self.hasFood = False
                self.world.setFood(self.pos, 1)
                return True
        return False

    def __str__(self):
        return "hasFood = {} ; pos = {} ; dpos = {}".format(self.hasFood, self.pos, self.dpos)

class World:
    def __init__(self, size=np.ones(2,dtype=int)*10, baseWeight=3, dirWeight=2, pherWeight=2, foodWeight=2, foodProb=0.1):
        self.baseWeight = baseWeight
        self.dirWeight = dirWeight
        self.pherWeight = pherWeight
        self.foodWeight = foodWeight

        self.size = size
        self.food = np.matrix([[int(rd.random() <= foodProb) for i in range(self.size[1])] for j in range(self.size[0])])
        self.pher = np.zeros(size)

    def addPher(self, pos, q=1):
        self.pher[pos[0],pos[1]]=min(self.pher[pos[0],pos[1]]+q,1)

    def setFood(self, pos, food):
        self.food[pos[0],pos[1]] = food

    def getFood(self, pos):
        return self.food[pos[0],pos[1]]

    def getPher(self, pos):
        return self.pher[pos[0],pos[1]]

    def crop(self,pos):
        return np.minimum(np.maximum(pos, np.zeros(2)), self.size - 1).astype(int)

    def createAnts(self, nAnts):
        self.nAnts = nAnts
        self.ants = [Ant(np.array([rd.randint(0,self.size[0]-1),rd.randint(0,self.size[1]-1)],dtype=int),np.zeros(2),self) for i in range(self.nAnts)]

    def move(self):
        for ant in self.ants: ant.move()
        self.pher = np.maximum(self.pher - 0.04,np.zeros_like(self.pher))

class Color:
    food = np.array([255,0,0])
    pher = np.array([0,90,0])
    ant = np.array([0,0,255])
    background = np.array([255,255,255])

class Game:
    def __init__(self, world):
        self.unit = min(1200//world.size[0], 700//world.size[1])
        self.world = world
        pygame.init()
        self.screen = pygame.display.set_mode(self.world.size * self.unit)

    def getRect(self, pos, size = 1):
        rect = np.zeros(4)
        if size == 1:
            rect = np.array((pos[0], pos[1], 1, 1))
        else:
            offset = (1-size)/2
            rect = np.array((pos[0]+offset, pos[1]+offset, size, size))
        return (rect * self.unit).astype(int)

    def draw(self):
        self.screen.fill(Color.background)
        for i in range(self.world.size[0]): 
            for j in range(self.world.size[1]):
                color = Color.food * self.world.getFood((i,j)) + Color.pher * self.world.getPher((i,j))
                pygame.draw.rect(self.screen, color, self.getRect((i,j)))
        for ant in self.world.ants:
            pygame.draw.rect(self.screen, Color.ant, self.getRect(ant.pos,0.5))
        pygame.display.flip()

if __name__ == "__main__":
    numberOfAnts = 100
    foodProb = 0.15
    sizeOfGrid = np.array([40,40])

    world = World(size=sizeOfGrid,foodProb=foodProb)
    world.createAnts(100)
    game = Game(world)

    frameNumber = 0
    infoFrames = 100
    drawFrames = 1

    start = time.time()
    while True:
        frameNumber += 1
        world.move()
        if not frameNumber % infoFrames:
            duration = time.time() - start
            print(infoFrames // duration, " fps")
            start = time.time()
        if not frameNumber % drawFrames:
            game.draw()
